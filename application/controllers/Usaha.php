<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Usaha extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Pengeluaran_model');
        $this->load->model('Pendapatan_model');
		$this->load->model('Usaha_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main(){
		
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['datausaha'] = $this->Usaha_model->get_by_idusaha($data_session['id'])->result();

		if (empty($data['datausaha'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		}else{
		$data['main_content'] = 'usaha/detail';
		$data['page_title'] = 'Halaman Detail usaha';
		$this->load->view('template', $data);
		}
	}

	public function detail($id)
    {
        //var_dump($data_transaksi);
        
        $statistik_insight =  $this->Pengeluaran_model->get_insight_by_id($id);
        $data_statistik_insight = [];

        if ($statistik_insight) {
            foreach ($statistik_insight as $key) {
                $data_statistik_insight['periode'][] = date("F", strtotime($key->periode));
                $data_statistik_insight['insight_pendapatan'][] = is_null($key->insight_pendapatan) ? 0 : $key->insight_pendapatan;
                $data_statistik_insight['insight_pengeluaran'][] = is_null($key->insight_pengeluaran) ? 0 : $key->insight_pengeluaran;
            }
        } else {
            $data_statistik_insight['periode'] = [];
            $data_statistik_insight['insight_pendapatan'] = [];
            $data_statistik_insight['insight_pengeluaran'] = [];
        }
        $data['pendapatan'] = $this->Pendapatan_model->get_total_pendapatan_by_id($id);
        $data['pengeluaran'] = $this->Pengeluaran_model->get_total_pengeluaran_by_id($id);

        $data['id'] = $id;
        $data['transaksi'] = $this->Pengeluaran_model->get_transaksi_all();
        $data['periode'] = $data_statistik_insight['periode'];
        $data['insight_pendapatan'] = $data_statistik_insight['insight_pendapatan'];
        $data['insight_pengeluaran'] = $data_statistik_insight['insight_pengeluaran'];
        $data['main_content'] = 'usaha/detail';
        $data['page_title'] = 'Detail';
        $this->load->view('template', $data);

		//print_r($statistik_insight);
    }

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'usaha/main';
		$data['page_title'] = 'Halaman usaha';
        $this->load->view('template',$data);
	}

	public function create()
	{
	
		$data['main_content'] = 'usaha/create';
		$data['page_title'] = 'Halaman usaha';
        $this->load->view('template',$data);
	}


	public function json()
	{
		header('Content-Type: application/json');
		$usaha =  $this->Usaha_model->json();

		$data['draw'] = 0;
        $data['recordsTotal'] = $usaha == null ? [] : count($usaha);
        $data['recordsFiltered'] = $usaha == null ? [] : count($usaha);
        $data['data'] = $usaha == null ? [] : $usaha;
		
        echo json_encode($data);
	}


	public function create_action() 
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>'.validation_errors());
			redirect(site_url('usaha'));
			//echo validation_errors();
		} else {
			
			// Insert Data Default Teknik usaha
			$this->Usaha_model->insert($this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('usaha'));
		}
	}

	public function edit($id)
	{

		$row = $this->Usaha_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_usaha' => $row,
				'main_content' => 'usaha/update',
                'page_title' => 'Edit usaha'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('usaha'));
		}
	}
	
	public function update_action($id)
	{
		
		$usaha = $this->Usaha_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			 $this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>'.validation_errors());
            redirect(site_url('usaha'));
		} else {
			if (empty($usaha)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('usaha'));
			}
	
			
			$this->Usaha_model->update($id, $this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('usaha'));
		}
	}

	public function delete($id)
	{
		$row = $this->Usaha_model->get_by_id($id);

		if ($row) {
			$cek = $this->Usaha_model->cek($id);
			if ($cek) {
				//echo "isi";
				$this->session->set_flashdata('pesan', 'Data Tidak Dapat Di Hapus, Data Masih Digunakan');
				redirect(site_url('usaha'));
			} else {
				//echo "kosong";
				$this->Usaha_model->delete($id);
				$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
				redirect(site_url('usaha'));
			}
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('usaha'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
