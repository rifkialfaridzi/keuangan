<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in'))) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }


        $this->load->model('Usaha_model');
        $this->load->model('Pengeluaran_model');
        $this->load->model('Pendapatan_model');
       
        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        //var_dump($data_transaksi);
        
        $statistik_insight =  $this->Pengeluaran_model->get_insight_all();
        $data_statistik_insight = [];

        if ($statistik_insight) {
            foreach ($statistik_insight as $key) {
                $data_statistik_insight['periode'][] = date("F", strtotime($key->periode));
                $data_statistik_insight['insight_pendapatan'][] = is_null($key->insight_pendapatan) ? 0 : $key->insight_pendapatan;
                $data_statistik_insight['insight_pengeluaran'][] = is_null($key->insight_pengeluaran) ? 0 : $key->insight_pengeluaran;
            }
        } else {
            $data_statistik_insight['periode'] = [];
            $data_statistik_insight['insight_pendapatan'] = [];
            $data_statistik_insight['insight_pengeluaran'] = [];
        }
        $data['pendapatan'] = $this->Pendapatan_model->get_total_pendapatan();
        $data['pengeluaran'] = $this->Pengeluaran_model->get_total_pengeluaran();
        $data['transaksi'] = $this->Pengeluaran_model->get_transaksi_all();
        $data['periode'] = $data_statistik_insight['periode'];
        $data['insight_pendapatan'] = $data_statistik_insight['insight_pendapatan'];
        $data['insight_pengeluaran'] = $data_statistik_insight['insight_pengeluaran'];
        $data['main_content'] = 'admin/main';
        $data['page_title'] = 'Halaman Admin';
        $this->load->view('template', $data);
    }

    public function cek()
    {
        $data['pendapatan'] = $this->Pengeluaran_model->get_pengeluaran_by_type();
       // $data['pengeluaran'] = $this->Pengeluaran_model->get_total_pengeluaran();
        // $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth(6);
    print_r($data);
    }

    public function laporan_bukubesar()
	{
		//$periode = $this->input->get('periode');
		$pengeluaran =  $this->Pengeluaran_model->get_pengeluaran_by_type();
		$pendapatan =  $this->Pengeluaran_model->get_pendapatan_by_type();
		
        $transaksi =  $this->Pengeluaran_model->get_transaksi();
        //print_r($transaksi);
        $transaksi_final = [];
        foreach ($transaksi as $key) {
            
            if ($key->jenis == "pendapatan") {
                $data_transaksi['nama_usaha'] = $key->nama_usaha;
                $data_transaksi['kode'] = $key->kode;
                $data_transaksi['keterangan'] = $key->keterangan;
                $data_transaksi['debet'] = $key->nominal;
                $data_transaksi['kredit'] = 0;
                $data_transaksi['tanggal'] = $key->tanggal;
                $data_transaksi['jenis'] = $key->jenis;
            }else{
                $data_transaksi['nama_usaha'] = $key->nama_usaha;
                $data_transaksi['kode'] = $key->kode;
                $data_transaksi['keterangan'] = $key->keterangan;
                $data_transaksi['debet'] = 0;
                $data_transaksi['kredit'] = $key->nominal;
                $data_transaksi['tanggal'] = $key->tanggal;
                $data_transaksi['jenis'] = $key->jenis;
            }

            $transaksi_final[] = $data_transaksi;
        }

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_Buku_Besar.pdf";
		$this->pdf->load_view('laporan/laporan_bukubesar', ['data_pengeluaran' => $pengeluaran, 'data_pendapatan' => $pendapatan, 'transaksi_final' => $transaksi_final]);
	}

    public function laporan_bukubesar_usaha($id, $year)
	{
		//$periode = $this->input->get('periode');
        $usaha = $this->Usaha_model->get_by_id($id);
		$pengeluaran =  $this->Pengeluaran_model->get_pengeluaran_by_type_id($id, $year);
		$pendapatan =  $this->Pengeluaran_model->get_pendapatan_by_type_id($id, $year);
		
        $transaksi =  $this->Pengeluaran_model->get_transaksi_by_id($id, $year);
        //print_r($transaksi);
        $transaksi_final = [];
        foreach ($transaksi as $key) {
            
            if ($key->jenis == "pendapatan") {
                $data_transaksi['nama_usaha'] = $key->nama_usaha;
                $data_transaksi['kode'] = $key->kode;
                $data_transaksi['keterangan'] = $key->keterangan;
                $data_transaksi['debet'] = $key->nominal;
                $data_transaksi['kredit'] = 0;
                $data_transaksi['tanggal'] = $key->tanggal;
                $data_transaksi['jenis'] = $key->jenis;
            }else{
                $data_transaksi['nama_usaha'] = $key->nama_usaha;
                $data_transaksi['kode'] = $key->kode;
                $data_transaksi['keterangan'] = $key->keterangan;
                $data_transaksi['debet'] = 0;
                $data_transaksi['kredit'] = $key->nominal;
                $data_transaksi['tanggal'] = $key->tanggal;
                $data_transaksi['jenis'] = $key->jenis;
            }

            $transaksi_final[] = $data_transaksi;
        }
        $tahun = $year;

        //print_r($usaha->nama);
       
        //print_r($transaksi_final);

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_Buku_Besar.pdf";
		$this->pdf->load_view('laporan/laporan_bukubesar_usaha', ['data_pengeluaran' => $pengeluaran, 'data_pendapatan' => $pendapatan, 'transaksi_final' => $transaksi_final, 'usaha' => $usaha, 'tahun' => $tahun]);
	}

    public function dashboard($month)
    {

        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->jumlah_produk_order_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);
    }
}
