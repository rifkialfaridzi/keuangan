<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pendapatan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Pendapatan_model');
		$this->load->model('Pengeluaran_model');
		$this->load->model('Usaha_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function generate_code()
	{
		$query = "SELECT max(kode) as maxKode FROM transaksi";
		$max_code = $this->db->query($query)->row()->maxKode;
		$noUrut = (int) substr($max_code, 4, 4);
		$noUrut++;
		$char = "TRX";
		$kodeBarang = $char . sprintf("%04s", $noUrut);
		return $kodeBarang;
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['datapendapatan'] = $this->Pendapatan_model->get_by_idpendapatan($data_session['id'])->result();

		if (empty($data['datapendapatan'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		} else {
			$data['main_content'] = 'pendapatan/detail';
			$data['page_title'] = 'Halaman Detail pendapatan';
			$this->load->view('template', $data);
		}
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['pendapatan'] = $this->Pendapatan_model->get_total_pendapatan();
		$data['main_content'] = 'pendapatan/main';
		$data['page_title'] = 'Halaman pendapatan';
		$this->load->view('template', $data);
	}

	public function create()
	{
		$data['usaha'] = $this->Usaha_model->get_all();
		$data['main_content'] = 'pendapatan/create';
		$data['page_title'] = 'Halaman pendapatan';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');

		$periode = $this->input->get('periode');
		$pendapatan =  $this->Pendapatan_model->get_transaksi_pendapatan_by_month($periode);



		$data['draw'] = 0;
		$data['recordsTotal'] = $pendapatan == null ? [] : count($pendapatan);
		$data['recordsFiltered'] = $pendapatan == null ? [] : count($pendapatan);
		$data['data'] = $pendapatan == null ? [] : $pendapatan;

		echo json_encode($data);
	}

	public function json_by_id($id)
	{
		header('Content-Type: application/json');

		$periode = $this->input->get('periode');
		$pendapatan =  $this->Pendapatan_model->get_transaksi_pendapatan_by_month_id($periode,$id);

		$data['draw'] = 0;
		$data['recordsTotal'] = $pendapatan == null ? [] : count($pendapatan);
		$data['recordsFiltered'] = $pendapatan == null ? [] : count($pendapatan);
		$data['data'] = $pendapatan == null ? [] : $pendapatan;

		echo json_encode($data);
	}

	public function laporan_pendapatan()
	{
		$periode = $this->input->get('periode');
		$pendapatan =  $this->Pendapatan_model->get_transaksi_pendapatan_by_month($periode);
		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_Pendapatan.pdf";
		$this->pdf->load_view('laporan/laporan_pendapatan', ['data_pendapatan' => $pendapatan, 'periode' => $periode]);
	}

	public function laporan_pendapatan_by_id($id)
	{
		$periode = $this->input->get('periode');
		$pendapatan =  $this->Pendapatan_model->get_transaksi_pendapatan_by_month_id($periode,$id);
		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_Pendapatan.pdf";
		$this->pdf->load_view('laporan/laporan_pendapatan', ['data_pendapatan' => $pendapatan, 'periode' => $periode]);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('pendapatan'));
			//echo validation_errors();
		} else {

			$data = $this->input->post();
			$data['kode'] = $this->generate_code();
			$data['jenis'] = "pendapatan";
			$data['kategori'] = 0;
			$data['tanggal'] = date("Y-m-d H:i:s");

			// Insert Data Default Teknik pendapatan
			$this->Pendapatan_model->insert($data);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('pendapatan'));
		}
	}

	public function edit($id)
	{

		$row = $this->Pendapatan_model->get_by_id($id);

		if ($row) {
			$data = array(
				'usaha' => $this->Usaha_model->get_all(),
				'data_pendapatan' => $row,
				'main_content' => 'pendapatan/update',
				'page_title' => 'Edit pendapatan'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('pendapatan'));
		}
	}

	public function update_action($id)
	{

		$pendapatan = $this->Pendapatan_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('pendapatan'));
		} else {
			if (empty($pendapatan)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('pendapatan'));
			}


			$this->Pendapatan_model->update($id, $this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('pendapatan'));
		}
	}

	public function delete($id)
	{
		$row = $this->Pendapatan_model->get_by_id($id);

		if ($row) {
			$this->Pendapatan_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pendapatan'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pendapatan'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nominal', 'Nominal', 'required');
		$this->form_validation->set_rules('usaha', 'Unit Usaha', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
