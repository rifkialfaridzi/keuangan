<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Pengeluaran_model');
		$this->load->model('Pengeluaran_model');
		$this->load->model('Kategori_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Usaha_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function generate_code()
	{
		$query = "SELECT max(kode) as maxKode FROM transaksi";
		$max_code = $this->db->query($query)->row()->maxKode;
		$noUrut = (int) substr($max_code, 4, 4);
		$noUrut++;
		$char = "TRX";
		$kodeBarang = $char . sprintf("%04s", $noUrut);
		return $kodeBarang;
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['datapengeluaran'] = $this->Pengeluaran_model->get_by_idpengeluaran($data_session['id'])->result();

		if (empty($data['datapengeluaran'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		} else {
			$data['main_content'] = 'pengeluaran/detail';
			$data['page_title'] = 'Halaman Detail pengeluaran';
			$this->load->view('template', $data);
		}
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'pengguna/main';
		$data['page_title'] = 'Halaman Pengguna';
		$this->load->view('template', $data);
	}

	public function create()
	{
		$data['main_content'] = 'pengguna/create';
		$data['page_title'] = 'Halaman Pengguna';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');


		$pengeluaran =  $this->Pengguna_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $pengeluaran == null ? [] : count($pengeluaran);
		$data['recordsFiltered'] = $pengeluaran == null ? [] : count($pengeluaran);
		$data['data'] = $pengeluaran == null ? [] : $pengeluaran;

		echo json_encode($data);
	}

	public function coba()
	{

		$statistik_insight =  $this->Pengeluaran_model->get_insight_all();
		$data_statistik_insight = [];

		if ($statistik_insight) {
			foreach ($statistik_insight as $key) {
				$data_statistik_insight['periode'][] = date("F", strtotime($key->periode));
				$data_statistik_insight['insight_pendapatan'][] = is_null($key->insight_pendapatan) ? 0 : $key->insight_pendapatan;
				$data_statistik_insight['insight_pengeluaran'][] = is_null($key->insight_pengeluaran) ? 0 : $key->insight_pengeluaran;
			}
		} else {
			$data_statistik_insight['periode'] = [];
			$data_statistik_insight['insight_klik'] = [];
			$data_statistik_insight['insight_quotation'] = [];
		}

		print_r($data_statistik_insight);
	}

	public function laporan_pengeluaran()
	{
		$periode = $this->input->get('periode');
		$pengeluaran =  $this->Pengeluaran_model->get_transaksi_pengeluaran_by_month($periode);
		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_pengeluaran.pdf";
		$this->pdf->load_view('laporan/laporan_pengeluaran', ['data_pengeluaran' => $pengeluaran, 'periode' => $periode]);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('pengguna'));
			//echo validation_errors();
		} else {

			$data_pengguna['username'] = $this->input->post("username");
			$data_pengguna['name'] = $this->input->post("nama");
			$data_pengguna['password'] = md5($this->input->post("password"));
			$data_pengguna['level'] = $this->input->post("jabatan");
			$data_pengguna['created_at'] = date("Y-m-d");

			// Insert Data pengguna
			$lastest_id = $this->Pengguna_model->insert($data_pengguna);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('pengguna'));
		}
	}



	public function edit($id)
	{

		$row = $this->Pengguna_model->get_by_id($id);
		//print_r($row);

		if ($row) {
			$data = array(
				'data_pengguna' => $row,
				'main_content' => 'pengguna/update',
				'page_title' => 'Edit pengguna'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('pengeluaran'));
		}
	}

	public function update_action($id)
	{

		$pengguna = $this->Pengguna_model->get_by_id($id);
		$is_username = $this->input->post('username', TRUE) != $pengguna->username ? '|is_unique[user.username]' : '';
		$this->form_validation->set_rules('username', 'Username', 'required' . $is_username);

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('pengguna/edit/' . $id));
		} else {

			if (empty($this->input->post('password'))) {
				$data = ['level' => $this->input->post('jabatan'),'name' => $this->input->post('nama'),'username' => $this->input->post('username')];
				//var_dump($data);
				$this->Pengguna_model->update($id, $data);
			} else {
				$data = ['level' => $this->input->post('jabatan'),'name' => $this->input->post('nama'),'username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))];
				//var_dump($data)
				$this->Pengguna_model->update($id, $data);
			}

			$this->session->set_flashdata('pesan', 'Data Sukses Diubah');
			redirect(site_url('pengguna/edit/' . $id));
		}
	}

	public function delete($id)
	{
		$row = $this->Pengguna_model->get_by_id($id);

		if ($row) {
			$this->Pengguna_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pengguna'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pengguna'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
