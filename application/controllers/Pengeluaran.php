<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pengeluaran extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('pengeluaran_model');
		$this->load->model('Pengeluaran_model');
		$this->load->model('Kategori_model');
		$this->load->model('Usaha_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function generate_code()
	{
		$query = "SELECT max(kode) as maxKode FROM transaksi";
		$max_code = $this->db->query($query)->row()->maxKode;
		$noUrut = (int) substr($max_code, 4, 4);
		$noUrut++;
		$char = "TRX";
		$kodeBarang = $char . sprintf("%04s", $noUrut);
		return $kodeBarang;
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['datapengeluaran'] = $this->Pengeluaran_model->get_by_idpengeluaran($data_session['id'])->result();

		if (empty($data['datapengeluaran'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		} else {
			$data['main_content'] = 'pengeluaran/detail';
			$data['page_title'] = 'Halaman Detail pengeluaran';
			$this->load->view('template', $data);
		}
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}


		$data['pengeluaran'] = $this->Pengeluaran_model->get_total_pengeluaran();
		$data['main_content'] = 'pengeluaran/main';
		$data['page_title'] = 'Halaman Pengeluaran';
		$this->load->view('template', $data);
	}

	public function create()
	{
		$data['usaha'] = $this->Usaha_model->get_all();
		$data['kategori'] = $this->Kategori_model->get_all();
		$data['main_content'] = 'pengeluaran/create';
		$data['page_title'] = 'Halaman pengeluaran';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');

		$periode = $this->input->get('periode');
		$pengeluaran =  $this->Pengeluaran_model->get_transaksi_pengeluaran_by_month($periode);

		$data['draw'] = 0;
		$data['recordsTotal'] = $pengeluaran == null ? [] : count($pengeluaran);
		$data['recordsFiltered'] = $pengeluaran == null ? [] : count($pengeluaran);
		$data['data'] = $pengeluaran == null ? [] : $pengeluaran;

		echo json_encode($data);
	}

	public function json_by_id($id)
	{
		header('Content-Type: application/json');

		$periode = $this->input->get('periode');
		$pengeluaran =  $this->Pengeluaran_model->get_transaksi_pengeluaran_by_month_id($periode,$id);

		$data['draw'] = 0;
		$data['recordsTotal'] = $pengeluaran == null ? [] : count($pengeluaran);
		$data['recordsFiltered'] = $pengeluaran == null ? [] : count($pengeluaran);
		$data['data'] = $pengeluaran == null ? [] : $pengeluaran;

		echo json_encode($data);
	}

	public function coba(){

		$statistik_insight =  $this->Pengeluaran_model->get_insight_all();
		$data_statistik_insight = [];

		if ($statistik_insight) {
			foreach ($statistik_insight as $key) {
				$data_statistik_insight['periode'][] = date("F",strtotime($key->periode));
				$data_statistik_insight['insight_pendapatan'][] = is_null($key->insight_pendapatan) ? 0 : $key->insight_pendapatan;
				$data_statistik_insight['insight_pengeluaran'][] = is_null($key->insight_pengeluaran) ? 0 : $key->insight_pengeluaran;
			}
		} else {
			$data_statistik_insight['periode']= [];
			$data_statistik_insight['insight_klik']= [];
			$data_statistik_insight['insight_quotation']= [];
		}

		print_r($data_statistik_insight);

	}

	public function laporan_pengeluaran()
	{
		$periode = $this->input->get('periode');
		$pengeluaran =  $this->Pengeluaran_model->get_transaksi_pengeluaran_by_month($periode);
		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_pengeluaran.pdf";
		$this->pdf->load_view('laporan/laporan_pengeluaran', ['data_pengeluaran' => $pengeluaran, 'periode' => $periode]);
	}

	public function laporan_pengeluaran_by_id($id)
	{
		$periode = $this->input->get('periode');
		$pengeluaran =  $this->Pengeluaran_model->get_transaksi_pengeluaran_by_month_id($periode, $id);
		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "Laporan_pengeluaran.pdf";
		$this->pdf->load_view('laporan/laporan_pengeluaran', ['data_pengeluaran' => $pengeluaran, 'periode' => $periode]);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('pengeluaran'));
			//echo validation_errors();
		} else {

			$data = $this->input->post();
			$data['kode'] = $this->generate_code();
			$data['jenis'] = "pengeluaran";
			$data['tanggal'] = date("Y-m-d H:i:s");

			// Insert Data Default Teknik pengeluaran
			$this->Pengeluaran_model->insert($data);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('pengeluaran'));
		}
	}

	public function edit($id)
	{

		$row = $this->Pengeluaran_model->get_by_id($id);

		if ($row) {
			$data = array(
				'usaha' => $this->Usaha_model->get_all(),
				'kategori' => $this->Kategori_model->get_all(),
				'data_pengeluaran' => $row,
				'main_content' => 'pengeluaran/update',
				'page_title' => 'Edit pengeluaran'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('pengeluaran'));
		}
	}

	public function update_action($id)
	{

		$pengeluaran = $this->Pengeluaran_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('pengeluaran'));
		} else {
			if (empty($pengeluaran)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('pengeluaran'));
			}


			$this->Pengeluaran_model->update($id, $this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('pengeluaran'));
		}
	}

	public function delete($id)
	{
		$row = $this->Pengeluaran_model->get_by_id($id);

		if ($row) {
			$this->Pengeluaran_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pengeluaran'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pengeluaran'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nominal', 'Nominal', 'required');
		$this->form_validation->set_rules('usaha', 'Unit Usaha', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori Pengeluaran', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
