<section class="section">
	<div class="section-header">
		<h1>Halaman Pemain</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Akun Pemain</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form method="POST" action="<?php echo base_url("pemain/update_akun/").$data_profile->id; ?>" class="needs-validation" novalidate="">
								<div class="form-group">
									<label for="nama">Username</label>
									<input id="username" value="<?php echo $data_profile->username; ?>" type="text" class="form-control" name="username" tabindex="1" placeholder="bambang" required autofocus>
									<div class="invalid-feedback">
										Username Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword4">Password</label>
									<input id="inputPassword4" placeholder="Password Default : password" type="text" min="8" maxlength="14"S class="form-control" name="password" tabindex="1" autofocus>
									<div class="invalid-feedback">
										Password Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Update
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header">
						<h4>Profil Pemain</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse1" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse1">
						<div class="card-body">
							<form method="POST" action="<?php echo base_url("pemain/update_profile/").$data_profile->id; ?>" class="needs-validation" novalidate="">
								<div class="form-group">
									<label for="name">Nama Pemain</label>
									<input id="name" type="text" value="<?php echo $data_profile->name; ?>" class="form-control" name="name" tabindex="1" placeholder="Bambang" required autofocus>
									<div class="invalid-feedback">
										Nama Pemain Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Tanggal Lahir</label>
									<input id="tanggal_lahir" value="<?php echo $data_profile->tanggal_lahir; ?>" type="text" class="form-control datepicker" name="tanggal_lahir" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Tanggal Lahir Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Nomor Punggung</label>
									<input id="nomor_punggung" type="number" value="<?php echo $data_profile->nomor_punggung; ?>" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="nomor_punggung" tabindex="1" placeholder="10" required autofocus>
									<div class="invalid-feedback">
										Nomor Punggung Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label>Pilih Posisi Pemain</label>
									<select name="posisi" class="form-control select2">
										<option value="">Pilih Posisi</option>
										<option <?php echo $data_profile->posisi == "Kiper" ? "selected" :""; ?> value="Kiper">Kiper</option>
										<option <?php echo $data_profile->posisi == "Back Kanan" ? "selected" :""; ?> value="Back Kanan">Back Kanan</option>
										<option <?php echo $data_profile->posisi == "Back Kiri" ? "selected" :""; ?> value="Back Kiri">Back Kiri</option>
										<option <?php echo $data_profile->posisi == "Center Back" ? "selected" :""; ?> value="Center Back">Center Back</option>
										<option <?php echo $data_profile->posisi == "Gelandang Kanan" ? "selected" :""; ?> value="Gelandang Kanan">Gelandang Kanan</option>
										<option <?php echo $data_profile->posisi == "Gelandang Tengah" ? "selected" :""; ?> value="Gelandang Tengah">Gelandang Tengah</option>
										<option <?php echo $data_profile->posisi == "Gelandang Kiri" ? "selected" :""; ?> value="Gelandang Kiri">Gelandang Kiri</option>
										<option <?php echo $data_profile->posisi == "Striker" ? "selected" :""; ?> value="Striker">Striker</option>
									</select>
								</div>
								<div class="form-group">
									<label for="nama">Alamat</label>
									<textarea id="alamat" type="text" class="form-control" name="alamat" tabindex="1" rows="4" cols="50" required autofocus><?php echo $data_profile->alamat; ?></textarea>
									<div class="invalid-feedback">
										Alamat Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Update
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<form method="POST" action="<?php echo base_url("pemain/update_kriteria/").$data_profile->id; ?>" class="needs-validation" novalidate="">
						<div class="card-header">
							<h4>Penilaian Teknik Fisik</h4>
						</div>
						<div class="card-body">
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="nama">Kecepatan</label>
									<input id="kecepatan" type="number" value="<?php echo $data_pemain->kecepatan; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="kecepatan" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Kecepatan Masih Kosong
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="nama">Ketepatan</label>
									<input id="ketepatan" type="number" value="<?php echo $data_pemain->ketepatan; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="ketepatan" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Ketepatan Masih Kosong
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="nama">Kekuatan</label>
									<input id="kekuatan" type="number" value="<?php echo $data_pemain->kekuatan; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="kekuatan" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Kekuatan Masih Kosong
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="nama">Kelincahan</label>
									<input id="kelincahan" type="number" value="<?php echo $data_pemain->kelincahan; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="kelincahan" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Kelincahan Masih Kosong
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="nama">Keseimbangan</label>
									<input id="keseimbangan" type="number" value="<?php echo $data_pemain->keseimbangan; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="keseimbangan" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Keseimbangan Masih Kosong
									</div>
								</div>
							</div>
						</div>
						<div class="card-header">
							<h4>Penilaian Teknik Dasar</h4>
						</div>
						<div class="card-body">
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="nama">Stoping</label>
									<input id="stoping" type="number" value="<?php echo $data_pemain->stoping; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="stoping" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Stoping Masih Kosong
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="nama">Kicking</label>
									<input id="kicking" type="number" value="<?php echo $data_pemain->kicking; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="kicking" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Kicking Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="nama">Dribbling</label>
									<input id="dribbling" type="number" value="<?php echo $data_pemain->dribbling; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="dribbling" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Dribbling Masih Kosong
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="nama">Heading</label>
									<input id="heading" type="number" value="<?php echo $data_pemain->heading; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="heading" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Heading Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="nama">Tackling</label>
									<input id="tackling" type="number" value="<?php echo $data_pemain->tackling; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="tackling" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Tackling Masih Kosong
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="nama">Goal Keeping</label>
									<input id="goal_keeping" type="number" value="<?php echo $data_pemain->goal_keeping; ?>" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="goal_keeping" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Goal Keeping Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
									Update
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Pemain</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#category_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pemain/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "id"
				},
				{
					"data": "nama"
				},
				{
					"data": "no_rekening"
				},
				{
					"data": "no_rekening"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("master/pemain/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("pemain/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>