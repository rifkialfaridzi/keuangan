<section class="section">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-success">
          <i class="fas fa-money-bill"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Pendapatan</h4>
          </div>
          <div class="card-body">
            <?php echo "Rp. " . number_format($pendapatan->total_pendapatan, 0, ',', '.'); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-danger">
          <i class="fas fa-money-bill"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Pengeluaran</h4>
          </div>
          <div class="card-body">
            <?php echo "Rp. " . number_format($pengeluaran->total_pengeluaran, 0, ',', '.'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Pendapatan VS Pengeluaran</h4>
          <div class="card-header-action">
            <a href="<?php //echo base_url('admin/laporan_bukubesar'); ?>" class="btn btn-primary">
              <i class="fa fa-print"></i> Laporan Buku Besar
            </a>
          </div>
        </div>
        <div class="card-body">
          <canvas id="myCharts" height="158"></canvas>
        </div>
      </div>
    </div>
  </div> -->
  <div class="row">
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <h4>Pendapatan</h4>
          <div class="card-header-action">
            <a href="<?php echo base_url('admin/laporan_bukubesar'); ?>" class="btn btn-primary">
              <i class="fa fa-print"></i> Laporan Buku Besar
            </a>
          </div>
        </div>
        <div class="card-body">
          <canvas id="myCharts1" height="158"></canvas>
        </div>
      </div>
    </div>
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <h4>Pengeluaran</h4>
          <div class="card-header-action">
            <a href="<?php echo base_url('admin/laporan_bukubesar'); ?>" class="btn btn-primary">
              <i class="fa fa-print"></i> Laporan Buku Besar
            </a>
          </div>
        </div>
        <div class="card-body">
          <canvas id="myCharts2" height="158"></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Transaksi Terakhir</h4>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>Kode</th>
                <th>Jenis</th>
                <th>Usaha</th>
                <th>Nominal</th>
                <th>Tanggal</th>
              </tr>
              <?php $i = 0;
              foreach ($transaksi as $key) {
                # code...
              ?>
                <tr>
                  <td><?php echo $key->kode; ?></td>
                  <td class="font-weight-600">
                    <?php if ($key->jenis == "pendapatan") { ?>
                      <div class="badge badge-success"><?php echo $key->jenis; ?></div>
                    <?php } else { ?>
                      <div class="badge badge-danger"><?php echo $key->jenis; ?></div>
                    <?php } ?>
                  </td>
                  <td class="font-weight-600"><?php echo $key->nama_usaha; ?></td>
                  <td class="font-weight-600"><?php echo "Rp. " . number_format($key->nominal, 0, ',', '.'); ?></td>
                  <td><?php $time = strtotime($key->tanggal);
                      echo date('F j, Y', $time); ?></td>
                  <!-- <td><a href="<?php //echo site_url("lineup/detail/" . $key->id) 
                                    ?>" class="btn btn-icon btn-primary"><i class="far fa-eye"></i></a></td> -->
                </tr>

              <?php if (++$i > 4) break;
              } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script>

var ctx1 = document.getElementById("myCharts1").getContext('2d');
var myChart1 = new Chart(ctx1, {
	type: 'bar',
	data: {
		labels: <?php echo json_encode($periode); ?>,
		datasets: [{
			label: 'Pendapatan',
			data: <?php echo json_encode($insight_pendapatan); ?>,
			backgroundColor: [
			'rgba(255, 99, 132, 0.2)',
			'rgba(255, 99, 132, 0.2)',
			'rgba(255, 99, 132, 0.2)',
      'rgba(255, 99, 132, 0.2)',
			'rgba(255, 99, 132, 0.2)',
			'rgba(255, 99, 132, 0.2)'
			],
			borderColor: [
			
			],
		borderWidth: 1
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

var ctx2 = document.getElementById("myCharts2").getContext('2d');
var myChart2 = new Chart(ctx2, {
	type: 'bar',
	data: {
		labels: <?php echo json_encode($periode); ?>,
		datasets: [{
			label: 'Pendapatan',
			data: <?php echo json_encode($insight_pengeluaran); ?>,
			backgroundColor: [
			'rgba(153, 102, 255, 0.2)',
			'rgba(153, 102, 255, 0.2)',
			'rgba(153, 102, 255, 0.2)',
      'rgba(153, 102, 255, 0.2)',
			'rgba(153, 102, 255, 0.2)',
			'rgba(153, 102, 255, 0.2)',
			],
			borderColor: [
			
			],
		borderWidth: 1
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});








  var ctx = document.getElementById("myCharts").getContext('2d');


  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: <?php echo json_encode($periode); ?>,
      datasets: [{
        label: 'Pengeluaran',
        data: <?php echo json_encode($insight_pengeluaran); ?>,
        borderWidth: 2,
        backgroundColor: 'rgba(254,86,83,.9)',
        borderWidth: 0,
        borderColor: 'transparent',
        pointBorderWidth: 0,
        pointRadius: 3.5,
        pointBackgroundColor: 'transparent',
        pointHoverBackgroundColor: 'rgba(254,86,83,.8)',
      }, {
        label: 'Pendapatan',
        data: <?php echo json_encode($insight_pendapatan); ?>,
        borderWidth: 2,
        backgroundColor: 'rgba(63,82,227,.8)',
        borderWidth: 0,
        borderColor: 'transparent',
        pointBorderWidth: 0,
        pointRadius: 3.5,
        pointBackgroundColor: 'transparent',
        pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
      }]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          gridLines: {
            // display: false,
            drawBorder: false,
            color: '#f2f2f2',
          },
          ticks: {
            beginAtZero: true,
            stepSize: 500000,
            callback: function(value, index, values) {
              return 'Rp. ' + value;
            }
          }
        }],
        xAxes: [{
          gridLines: {
            display: false,
            tickMarkLength: 15,
          }
        }]
      },
    }
  });
</script>