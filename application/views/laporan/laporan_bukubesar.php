<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Buku Besar</title>
</head>

<body>
    <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
                <td style="text-align:center">
                    <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
                    <br>
                    <br>
                    <h1>BUMDES KRIDHA JAYA</h1>
                    <small>Kantor Desa Puntukrejo Kecamatan Ngargoyoso</small>
                </td>
            </tr>
        </tbody>

    </table>

    <hr>

    <div style="text-align:center">

        <p>&nbsp;</p>
        <h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">A. Transaksi</h4>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Unit Usaha</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Keterangan</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Debet</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kredit</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Saldo</b></td>
                </tr>

                <?php $balance = 0; foreach ($transaksi_final as $key) {
                ?>
                    <tr>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo date("d-m-Y", strtotime($key["tanggal"])); ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key["nama_usaha"]; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key["keterangan"]; ?></td>
                        <td style="text-align:right; padding: 2px 5px 2px 5px"><?php echo "Rp. " . number_format($key["debet"], 0, ',', '.'); ?></td> 
                        <td style="text-align:right; padding: 2px 5px 2px 5px"><?php echo "Rp. " . number_format($key["kredit"], 0, ',', '.'); ?></td>
                        <td style="text-align:right; padding: 2px 5px 2px 5px">
                        <?php 
                        
                        if ($key["jenis"] == "pendapatan") {
                           $balance += $key["debet"];
                        }else{
                            $balance -= $key["kredit"];
                        }
                        echo "Rp. " . number_format($balance, 0, ',', '.');

                        ?>
                        </td>
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>

        <h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">B. Data Pendapatan</h4>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kode</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Keterangan</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px;width:20%"><b>Nominal</b></td>
                </tr>

                <?php foreach ($data_pendapatan as $key) {
                ?>
                    <tr>
                        <td style="text-align:left; padding: 2px 5px 2px 5px">KD0<?php echo $key->id_usaha; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->keterangan; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo "Rp. " . number_format($key->jumlah_nominal, 0, ',', '.'); ?></td>
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>

        <h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">C. Data Pengeluaran</h4>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kode</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Keterangan</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px;width:20%"><b>Nominal</b></td>
                </tr>

                <?php foreach ($data_pengeluaran as $key) {
                ?>
                    <tr>
                        <td style="text-align:left; padding: 2px 5px 2px 5px">KD0<?php echo $key->id_usaha; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->keterangan; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px;"><?php echo "Rp. " . number_format($key->jumlah_nominal, 0, ',', '.'); ?></td>
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>

        <p>&nbsp;</p>

        <p>&nbsp;</p>

        <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:right;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td>Yang Mengetahui,</td>
                </tr>
                <tr>
                    <!-- <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td> -->
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Drs. Suparno <br> Kepala Desa</td>
                </tr>
            </tbody>
        </table>
        <br>
        <p>Di Cetak Pada : <?php echo date('Y-m-d H:i:s'); ?></p>
    </div>
</body>

</html>