<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Pengeluaran</title>
</head>
<body>
<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

<tbody>
    <tr>
        <td style="text-align:center">
        <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
            <h1>BUMDES KRIDHA JAYA</h1>
            <small>Kantor Desa Puntukrejo Kecamatan Ngargoyoso</small>
            <h4>Laporan Pengeluaran Periode <?php echo date("F") ?></h4>
        </td>
    </tr>
</tbody>

</table>

<hr>

<div style="text-align:center">

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>

            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kode</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Unit Usaha</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kategori</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Ketrangan</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nominal</b></td>
        </tr>

        <?php $totalBiaya = 0; foreach ($data_pengeluaran as $key) { 
            $totalBiaya += $key->nominal;
        ?>
        <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->kode; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->nama_usaha; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->nama_kategori; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->keterangan; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->tanggal; ?></td>
            <td style="text-align:right; padding: 2px 5px 2px 5px"><?php echo "Rp. ". number_format($key->nominal, 0, ',', '.'); ?></td>
        </tr>
        <?php } 
        ?>
        <tr>
            <td colspan="5" style="text-align:center; font-weight: bold;"> Total </td>
            <td style="text-align:right; font-weight: bold;padding: 2px 5px 2px 5px"> <?php echo "Rp. ". number_format($totalBiaya, 0, ',', '.'); ?> </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:right;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>
            <td>Yang Mengetahui,</td>
        </tr>
        <tr>
            <!-- <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td> -->
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
        <td>Drs. Suparno <br> Kepala Desa</td>
        </tr>
    </tbody>
</table>
<br>
        <p>Di Cetak Pada : <?php echo date('Y-m-d H:i:s'); ?></p>
</div>
</body>
</html>