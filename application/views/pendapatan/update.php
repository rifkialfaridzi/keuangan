<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Ubah pendapatan</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Pendapatan : <?php echo $data_pendapatan->kode; ?> - <?php echo $data_pendapatan->tanggal; ?></h4>
					</div>
					<div class="card-body">
						<form method="POST" action="<?php echo base_url("pendapatan/update_action/").$data_pendapatan->id; ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Unit Usaha</label>
								<div class="col-sm-12 col-md-7">
									<select class="form-control select2" name="usaha" id="" required autofocus>
										<option value="">Pilih Unit Usaha</option>

										<?php foreach ($usaha as $key) { ?>
											<option <?php echo $data_pendapatan->usaha == $key->id ? "selected" : ""; ?>  value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
										<?php } ?>

									</select>
									<div class="invalid-feedback">
										Unit Usaha Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nominal</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="number" class="form-control" value="<?php echo $data_pendapatan->nominal; ?>" name="nominal" tabindex="1" placeholder="10000" required autofocus>
									<div class="invalid-feedback">
										Nominal Pendapatan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" value="<?php echo $data_pendapatan->keterangan; ?>" name="keterangan" tabindex="1" placeholder="Keterangan Singkat" required autofocus>
									<div class="invalid-feedback">
										Keterangan Pendapatan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Tambahkan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>