<section class="section">
	<!-- <div class="section-header">
		<h1>Halaman Pengeluaran</h1>
	</div> -->

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="balance-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-danger">
						<i class="fas fa-money-bill"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Pengeluaran</h4>
						</div>
						<div class="card-body">
							<?php echo "Rp. " . number_format($pengeluaran->total_pengeluaran, 0, ',', '.'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="balance-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-danger">
						<i class="fas fa-file-alt"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Jumlah Transaksi</h4>
						</div>
						<div class="card-body">
							<?php echo $pengeluaran->jumlah_transkasi_pengeluaran; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Pengeluaran</h4>
						<?php if ($this->session->userdata['level'] != "3" && $this->session->userdata['level'] != "4") {  ?>
							<div class="card-header-action">
								<a href="<?php echo base_url('pengeluaran/create'); ?>" class="btn btn-primary">
									<i class="fa fa-plus"></i> Tambah Pengeluaran
								</a>
							</div>
						<?php } ?>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Pilih Data</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="input-group-text">
										<i class="fas fa-calendar"></i>
									</div>
								</div>
								<select name="periode" id="periode" class="form-control select2">
									<option value="">Pilih Periode</option>
									<option value="1">Januari</option>
									<option value="2">Februari</option>
									<option value="3">Maret</option>
									<option value="4">April</option>
									<option value="5">Mei</option>
									<option value="6">Juni</option>
									<option value="7">Juli</option>
									<option value="8">Agustus</option>
									<option value="9">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
								<div class="input-group-prepend">
									<div class="input-group-text">
										<a id="cetak" href="#" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
									</div>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table id="tabel_pengeluaran" class="table table-striped">
								<thead>
									<tr>
										<th>Kode</th>
										<th>Unit Usaha</th>
										<th>Kategori</th>
										<th>Keterangan</th>
										<th>Nominal</th>
										<th>Tanggal</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Pengeluaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		counter = 1;
		table = $('#tabel_pengeluaran').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pengeluaran/json?periode=') . date("m"); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "kode"
				},
				{
					"data": "nama_usaha"
				},
				{
					"data": "nama_kategori"
				},
				{
					"data": "keterangan"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						row_nominal = row.nominal
						return "Rp. " + row_nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
					}
				},
				{
					"data": "tanggal"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if (<?php echo $this->session->userdata['level'] ?> != "3" && <?php echo $this->session->userdata['level'] ?> != "4") {
							return '<a href="<?php echo site_url("pengeluaran/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						} else {
							return "-";
						}
					}
				}
			],

		});

		$('#periode').on('change', function(e) {
			table.ajax.url("<?php echo site_url('pengeluaran/json?periode=') ?>" + this.value).load();
			$("#cetak").click(function() {
				window.open("<?php echo site_url('pengeluaran/laporan_pengeluaran?periode=') ?>" + $("#periode").val());
				// console.log(getFirstDate);
				// console.log(getLastDate);
			});
		});




	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("pengeluaran/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>