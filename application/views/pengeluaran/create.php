<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Tambah Pengeluaran Baru</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("pengeluaran/create_action"); ?>" class="needs-validation" novalidate="">

							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Unit Usaha</label>
								<div class="col-sm-12 col-md-7">
									<select class="form-control select2" name="usaha" id="" required autofocus>
										<option value="">Pilih Unit Usaha</option>

										<?php foreach ($usaha as $key) { ?>
											<option value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
										<?php } ?>

									</select>
									<div class="invalid-feedback">
										Unit Usaha Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kategori Pengeluaran</label>
								<div class="col-sm-12 col-md-7">
									<select class="form-control select2" name="kategori" id="" required autofocus>
										<option value="">Pilih Kategori</option>

										<?php foreach ($kategori as $key) { ?>
											<option value="<?php echo $key->id; ?>"><?php echo $key->nama; ?></option>
										<?php } ?>

									</select>
									<div class="invalid-feedback">
										Kategori Pengeluaran Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nominal</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="number" class="form-control" name="nominal" tabindex="1" placeholder="10000" required autofocus>
									<div class="invalid-feedback">
										Nominal pengeluaran Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="keterangan" tabindex="1" placeholder="Keterangan Singkat" required autofocus>
									<div class="invalid-feedback">
										Keterangan Pengeluaran Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Tambahkan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<script>
	var cleaveC = new Cleave('.currency', {
		numeral: true,
		numeralThousandsGroupStyle: 'thousand'
	});

	var cleave = new Cleave('.phone-numbers', {
		numericOnly: true,
		delimiters: [' ', ' ', ' '],
		blocks: [4, 4, 4]
	});

	var cleave = new Cleave('.nik-formating', {
		numericOnly: true,
		delimiters: ['.', '.', '.', '.', '.', '-'],
		blocks: [2, 2, 2, 2, 2, 2, 4]
	});
</script>