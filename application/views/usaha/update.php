<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Ubah Usaha</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah Usaha : <?php echo $data_usaha->nama;?></h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("usaha/update_action/".$data_usaha->id); ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Usaha</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="nama" value="<?php echo $data_usaha->nama;?>" tabindex="1" placeholder="<?php echo $data_usaha->nama;?>" required autofocus>
									<div class="invalid-feedback">
										Nama usaha Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="keterangan" value="<?php echo $data_usaha->Keterangan;?>" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Keterangan usaha Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Ubah</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>