<section class="section">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card card-statistic-2">
				<div class="card-chart">
					<canvas id="balance-chart" height="80"></canvas>
				</div>
				<div class="card-icon shadow-primary bg-success">
					<i class="fas fa-money-bill"></i>
				</div>
				<div class="card-wrap">
					<div class="card-header">
						<h4>Pendapatan</h4>
					</div>
					<div class="card-body">
						<?php echo "Rp. " . number_format($pendapatan->total_pendapatan, 0, ',', '.'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="card card-statistic-2">
				<div class="card-chart">
					<canvas id="balance-chart" height="80"></canvas>
				</div>
				<div class="card-icon shadow-primary bg-danger">
					<i class="fas fa-money-bill"></i>
				</div>
				<div class="card-wrap">
					<div class="card-header">
						<h4>Pengeluaran</h4>
					</div>
					<div class="card-body">
						<?php echo "Rp. " . number_format($pengeluaran->total_pengeluaran, 0, ',', '.'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Pendapatan VS Pengeluaran</h4>
          <div class="card-header-action">
            <a href="<?php //echo base_url('admin/laporan_bukubesar'); 
						?>" class="btn btn-primary">
              <i class="fa fa-print"></i> Laporan Buku Besar
            </a>
          </div>
        </div>
        <div class="card-body">
          <canvas id="myCharts" height="158"></canvas>
        </div>
      </div>
    </div>
  </div> -->
	<div class="row">
		<div class="col-12 mb-4">
			<div class="hero bg-success text-white">
				<div class="hero-inner">
					<h2>Download Laporan Tahunan</h2>
					<p class="lead">Download Laporan Tahunan Disini</p>
					<div class="mt-4">
						<div class="form-group">
							<div class="input-group mb-3">
								<input type="text" id="datepicker" class="form-control" placeholder="" aria-label="">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button">Download</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-header">
					<h4>Pendapatan</h4>
					<div class="card-header-action">
						<a href="<?php echo base_url('admin/laporan_bukubesar_usaha/') . $id; ?>" class="btn btn-primary">
							<i class="fa fa-print"></i> Laporan Buku Besar
						</a>
					</div>
				</div>
				<div class="card-body">
					<canvas id="myCharts1" height="158"></canvas>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="card">
				<div class="card-header">
					<h4>Pengeluaran</h4>
					<div class="card-header-action">
						<a href="<?php echo base_url('admin/laporan_bukubesar_usaha/') . $id; ?>" class="btn btn-primary">
							<i class="fa fa-print"></i> Laporan Buku Besar
						</a>
					</div>
				</div>
				<div class="card-body">
					<canvas id="myCharts2" height="158"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					<h4>Data Pendapatan</h4>
					<?php if ($this->session->userdata['level'] != "3" && $this->session->userdata['level'] != "4") {  ?>
						<div class="card-header-action">
							<a href="<?php echo base_url('pendapatan/create'); ?>" class="btn btn-primary">
								<i class="fa fa-plus"></i> Tambah Pendapatan
							</a>
						</div>
					<?php } ?>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label>Pilih Data</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fas fa-calendar"></i>
								</div>
							</div>
							<select name="periode" id="periode" class="form-control select2">
								<option value="">Pilih Periode</option>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
							<div class="input-group-prepend">
								<div class="input-group-text">
									<a id="cetak" href="#" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="tabel_pendapatan" class="table table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Unit Usaha</th>
									<th>Nominal</th>
									<th>Tanggal</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					<h4>Data Pengeluaran</h4>
					<?php if ($this->session->userdata['level'] != "3" && $this->session->userdata['level'] != "4") {  ?>
						<div class="card-header-action">
							<a href="<?php echo base_url('pengeluaran/create'); ?>" class="btn btn-primary">
								<i class="fa fa-plus"></i> Tambah Pengeluaran
							</a>
						</div>
					<?php } ?>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label>Pilih Data</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fas fa-calendar"></i>
								</div>
							</div>
							<select name="periode" id="periode1" class="form-control select2">
								<option value="">Pilih Periode</option>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
							<div class="input-group-prepend">
								<div class="input-group-text">
									<a id="cetak1" href="#" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="tabel_pengeluaran" class="table table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Unit Usaha</th>
									<th>Kategori</th>
									<th>Keterangan</th>
									<th>Nominal</th>
									<th>Tanggal</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	var ctx1 = document.getElementById("myCharts1").getContext('2d');
	var myChart1 = new Chart(ctx1, {
		type: 'bar',
		data: {
			labels: <?php echo json_encode($periode); ?>,
			datasets: [{
				label: 'Pendapatan',
				data: <?php echo json_encode($insight_pendapatan); ?>,
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)'
				],
				borderColor: [

				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	var ctx2 = document.getElementById("myCharts2").getContext('2d');
	var myChart2 = new Chart(ctx2, {
		type: 'bar',
		data: {
			labels: <?php echo json_encode($periode); ?>,
			datasets: [{
				label: 'Pendapatan',
				data: <?php echo json_encode($insight_pengeluaran); ?>,
				backgroundColor: [
					'rgba(153, 102, 255, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(153, 102, 255, 0.2)',
				],
				borderColor: [

				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});


	$(document).ready(function() {
		//datatables
		counter = 1;
		table = $('#tabel_pengeluaran').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pengeluaran/json_by_id/') . $id . "?periode=" . date("m"); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "kode"
				},
				{
					"data": "nama_usaha"
				},
				{
					"data": "nama_kategori"
				},
				{
					"data": "keterangan"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						row_nominal = row.nominal
						return "Rp. " + row_nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
					}
				},
				{
					"data": "tanggal"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if (<?php echo $this->session->userdata['level'] ?> != "3" && <?php echo $this->session->userdata['level'] ?> != "4") {
							return '<a href="<?php echo site_url("pengeluaran/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						} else {
							return "-";
						}
					}
				}
			],

		});

		$('#periode1').on('change', function(e) {
			table.ajax.url("<?php echo site_url('pengeluaran/json_by_id/') . $id . "?periode="; ?>" + this.value).load();
			$("#cetak1").click(function() {
				
				window.open("<?php echo site_url('pengeluaran/laporan_pengeluaran_by_id/') . $id . "?periode="; ?>" + $("#periode1").val());
				// console.log(getFirstDate);
				// console.log(getLastDate);
			});
		});




	});


	//datatables
	counter = 1;
	table1 = $('#tabel_pendapatan').DataTable({
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?php echo site_url('pendapatan/json_by_id/') . $id . "?periode=" . date("m"); ?>',
			"type": "POST"
		},
		//Set column definition initialisation properties.
		"columns": [{
				"data": "kode"
			},
			{
				"data": "nama_usaha"
			},
			{
				"data": null,
				"render": function(data, type, row) {
					row_nominal = row.nominal
					return "Rp. " + row_nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
				}
			},
			{
				"data": "tanggal"
			},
			{
				"data": null,
				"render": function(data, type, row) {
					if (<?php echo $this->session->userdata['level'] ?> != "3" && <?php echo $this->session->userdata['level'] ?> != "4") {
						return '<a href="<?php echo site_url("pendapatan/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					} else {
						return "-";
					}
				}
			}
		],

	});

	$('#periode').on('change', function(e) {
		table1.ajax.url("<?php echo site_url('pendapatan/json_by_id/') . $id . "?periode="; ?>" + this.value).load();
		$("#cetak").click(function() {
			window.open("<?php echo site_url('pendapatan/laporan_pendapatan_by_id/') . $id . "?periode="; ?>" + $("#periode").val());
			// console.log(getFirstDate);
			// console.log(getLastDate);
		});
	});

	// $('#datepicker').daterangepicker({
	// 	singleDatePicker: true,
	// 	showDropdowns: true,
	// 	format: 'YYYY'
	// });

	// $('#datepicker').daterangepicker({
    // format: "yyyy",
    // autoclose: true,
    // minViewMode: "years"});
	$("#datepicker").datepicker({
            autoclose: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        }).on("change",function(){
			alert("Download Laporan Tahun "+this.value);
			window.open("<?php echo site_url('admin/laporan_bukubesar_usaha/'); ?>" +<?php echo $id; ?>+"/"+ this.value);
		});
</script>