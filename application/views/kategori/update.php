<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Ubah kategori</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah kategori : <?php echo $data_kategori->nama;?></h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("kategori/update_action/".$data_kategori->id); ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama kategori</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="nama" value="<?php echo $data_kategori->nama;?>" tabindex="1" placeholder="<?php echo $data_kategori->nama;?>" required autofocus>
									<div class="invalid-feedback">
										Nama kategori Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="keterangan" value="<?php echo $data_kategori->keterangan;?>" tabindex="1" placeholder="<?php echo $data_kategori->keterangan;?>" required autofocus>
									<div class="invalid-feedback">
										Keterangan kategori Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Ubah</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>