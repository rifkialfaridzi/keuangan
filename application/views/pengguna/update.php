<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Edit Pegawai</h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Edit Data Pegawai</h4>
					</div>
					<div class="card-body">
						<form method="POST" action="<?php echo base_url("pengguna/update_action/") . $data_pengguna->id; ?>" enctype="multipart/form-data" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" value="<?php echo $data_pengguna->name; ?>" class="form-control" name="nama" tabindex="1" placeholder="Bambang" required autofocus>
									<div class="invalid-feedback">
										Nama pengguna Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jabatan</label>
								<div class="col-sm-12 col-md-7">
									<select name="jabatan" class="form-control selectric" required autofocus>
										<option value="">Pilih Jabatan</option>
										<option <?php echo $data_pengguna->level == "1" ? "selected" : ""; ?> value="1">Admin</option>
										<option <?php echo $data_pengguna->level == "2" ? "selected" : ""; ?> value="2">Pengelola</option>
										<option <?php echo $data_pengguna->level == "3" ? "selected" : ""; ?> value="3">Pimpinan</option>
										<option <?php echo $data_pengguna->level == "4" ? "selected" : ""; ?> value="4">Kepala Desa</option>
									</select>
									<div class="invalid-feedback">
										Nama pengguna Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Username</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control" value="<?php echo $data_pengguna->username; ?>" name="username" tabindex="1" placeholder="Bambang" required autofocus>
									<div class="invalid-feedback">
										Nama pengguna Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password</label>
								<div class="col-sm-12 col-md-7">
									<input type="password" class="form-control" name="password" tabindex="1">
									<div class="invalid-feedback">
										Password Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Ubah</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<script>

</script>