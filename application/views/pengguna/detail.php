<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Performance Marketing Partner</h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-danger">
						<i class="far fa-newspaper"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Kunjungan Halaman</h4>
						</div>
						<div class="card-body">
							42
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-warning">
						<i class="far fa-file"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Interaksi</h4>
						</div>
						<div class="card-body">
							1,201
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-success">
						<i class="fas fa-circle"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Quotation</h4>
						</div>
						<div class="card-body">
							47
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						<h4>Statistik Performance</h4>
					</div>
					<div class="card-body">
						<canvas id="myChart" height="158"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<h4>Negara Teratas</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-title mb-2">July</div>
								<ul class="list-unstyled list-unstyled-border list-unstyled-noborder mb-0">
									<li class="media">
										<img class="img-fluid mt-1 img-shadow" src="<?php echo base_url('assets/') ?>modules/flag-icon-css/flags/4x3/id.svg" alt="image" width="40">
										<div class="media-body ml-3">
											<div class="media-title">Indonesia</div>
											<div class="text-small text-muted">3,282 <i class="fas fa-caret-down text-danger"></i></div>
										</div>
									</li>
									<li class="media">
										<img class="img-fluid mt-1 img-shadow" src="<?php echo base_url('assets/') ?>modules/flag-icon-css/flags/4x3/my.svg" alt="image" width="40">
										<div class="media-body ml-3">
											<div class="media-title">Malaysia</div>
											<div class="text-small text-muted">2,976 <i class="fas fa-caret-down text-danger"></i></div>
										</div>
									</li>
									<li class="media">
										<img class="img-fluid mt-1 img-shadow" src="<?php echo base_url('assets/') ?>modules/flag-icon-css/flags/4x3/SG.svg" alt="image" width="40">
										<div class="media-body ml-3">
											<div class="media-title">Singapura</div>
											<div class="text-small text-muted">1,576 <i class="fas fa-caret-up text-success"></i></div>
										</div>
									</li>
									<li class="media">
										<img class="img-fluid mt-1 img-shadow" src="<?php echo base_url('assets/') ?>modules/flag-icon-css/flags/4x3/BN.svg" alt="image" width="40">
										<div class="media-body ml-3">
											<div class="media-title">Brunei Darussalam</div>
											<div class="text-small text-muted">1,723 <i class="fas fa-caret-up text-success"></i></div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Quotation</h4>
						<div class="card-header-action">

						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="tabel_quotation" class="table table-striped">
								<thead>
									<tr>
										<th>Refferal</th>
										<th>Customer</th>
										<th>Produk</th>
										<th>Jumlah</th>
										<th>Alamat</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<?php
$data_array_bulan = $data_statistik_insight['periode'];
$data_array_interaksi = $data_statistik_insight['insight_klik'];
$data_array_konversi = $data_statistik_insight['insight_quotation'];


?>

<script>
	const d = new Date();
	let months = d.getMonth() + 1;
	conter = 1


	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: <?php echo json_encode($data_array_bulan); ?>,
			datasets: [{
				label: 'Konversi',
				data: <?php echo json_encode($data_array_konversi); ?>,
				borderWidth: 2,
				backgroundColor: 'rgba(254,86,83,.9)',
				borderWidth: 0,
				borderColor: 'transparent',
				pointBorderWidth: 0,
				pointRadius: 3.5,
				pointBackgroundColor: 'transparent',
				pointHoverBackgroundColor: 'rgba(254,86,83,.8)',
			}, {
				label: 'Interaksi',
				data: <?php echo json_encode($data_array_interaksi); ?>,
				borderWidth: 2,
				backgroundColor: 'rgba(63,82,227,.7)',
				borderWidth: 0,
				borderColor: 'transparent',
				pointBorderWidth: 0,
				pointRadius: 3.5,
				pointBackgroundColor: 'transparent',
				pointHoverBackgroundColor: 'rgba(63,82,227,.7)',
			}, ]
		},
		options: {
			legend: {
				display: false
			},
			scales: {
				yAxes: [{
					gridLines: {
						// display: false,
						drawBorder: false,
						color: '#f2f2f2',
					},
					ticks: {
						beginAtZero: true,
						stepSize: 10,
						callback: function(value, index, values) {
							return value;
						}
					}
				}],
				xAxes: [{
					gridLines: {
						display: false,
						tickMarkLength: 15,
					}
				}]
			},
		}
	});

	table = $('#tabel_quotation').DataTable({
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": '<?php echo site_url('quotation/by_user/' . $id_user); ?>',
			"type": "POST"
		},
		//Set column definition initialisation properties.
		"columns": [{
				"data": null,
				"render": function(data, type, row) {
					if (row.nama_reff == null) {
						return "Unknown";
					} else {
						return row.nama_reff;
					}
				}
			},
			{
				"data": "nama"
			},
			{
				"data": "nama_produk"
			},
			{
				"data": null,
				"render": function(data, type, row) {
					jumlah = parseInt(row.jumlah);
					return jumlah.toFixed(0).replace(/(\d)(?=(\d{3})+\b)/g, '$1 ') + " /Liter";
				}
			},
			{
				"data": "alamat"
			},
			{
				"data": "created_at"
			},
			{
				"data": null,
				"render": function(data, type, row) {
					if (row.status == "belum dilihat") {
						return '<div class="badge badge-danger">' + row.status + '</div>'
					}
					return '<div class="badge badge-success">' + row.status + '</div>'
				}
			},
			{
				"data": null,
				"render": function(data, type, row) {
					return '<a href="<?php echo site_url("quotation/detail/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle"></i></a>';
				}
			}
		],

	});

	// $('#month').on('change', function() {


	// 	table.ajax.url("<?php //echo site_url('pegawai/data_absen/') . $id_user . "/"; 
							?>" + this.value).load();


	// });

	$(document).ready(function() {


		$('#months').on('change', function() {


			tables1.ajax.url("<?php echo site_url('pegawai/data_ijin/') . $id_user . "/"; ?>" + this.value).load();


		});

	});
</script>